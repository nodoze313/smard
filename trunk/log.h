/***************************************************************************
*   Copyright (C) 2008 by BluegrassNet                                    *
*   Written by James MacLachlan (james@bluegrass.net)                     *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#ifndef AUTORESPONDER_LOG_H
#define AUTORESPONDER_LOG_H

#ifndef EXEC
#define EXEC "autoresponder"
#endif

#ifdef DEBUG
#define DBGLOGCDE(_dbgmsg,_dbgcd) \
  openlog(EXEC,LOG_CONS|LOG_PID,LOG_MAIL); \
  syslog(LOG_DEBUG,_dbgmsg,_dbgcd); \
  closelog();
#define DBGLOGMSG(_dbgmsg) \
  openlog(EXEC,LOG_CONS|LOG_PID,LOG_MAIL); \
  syslog(LOG_DEBUG,_dbgmsg); \
  closelog();
#else
#define DBGLOGCDE 0;
#define DBGLOGMSG 0;
#endif

#define LOGWOCODE(_runtimeLogLevel,_priority,_sysmsg) \
  openlog(EXEC,LOG_CONS|LOG_PID,LOG_MAIL); \
  _runtimeLogLevel>_priority?(void)0:syslog(_priority,_sysmsg); \
  closelog();

#define LOGWCODE(_runtimeLogLevel,_priority,_sysmsg,_syscd) \
  openlog(EXEC,LOG_CONS|LOG_PID,LOG_MAIL); \
  _runtimeLogLevel>_priority?(void)0:syslog(_priority,_sysmsg,_syscd); \
  closelog();

#endif

