/***************************************************************************
*   Copyright (C) 2008 by BluegrassNet                                    *
*   Written by James MacLachlan (james@bluegrass.net)                     *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/
#include "worker.h"

worker::worker(const int* runtimeLogLevel,const char* config)
{
  this->config = config;
  this->runtimeLogLevel = runtimeLogLevel;
}

worker::~worker()
{
}

int worker::getUseCount()
{
  return this->useCount;
}
void worker::setUseCount(int useCount)
{
  boost::mutex::scoped_lock lock(this->countMutex);
  this->useCount = useCount;
}

int worker::initWorker()
{
  if(this->workerThread = new boost::thread(boost::bind(&worker::workerLoop,this)))
  {
    DBGLOGMSG("Generated worker thread.");
  }
  else
  {
    LOGWOCODE(*this->runtimeLogLevel,LOG_ERR,"Failed to create worker thread.");
    return 1;
  }
  return 0;
}

void* worker::workerLoop()
{
  mailInterface mi = mailInterface(config);
  userInterface ui = userInterface(this->config);
  timing ti = timing(&mi,&ui,this->config);
  while(1)
  {
    if(this->getUseCount() >= THREAD_LIFE)
    {
      DBGLOGCDE("Thread use: %i, thread exiting.",this->getUseCount());
      return 0;
    }
    if(ti.updateQueue())
    {
      //this should be the final exit point
      LOGWOCODE(*this->runtimeLogLevel,LOG_ERR,"Thread exiting, failed on updateQueue.");
      this->setUseCount(-1);
      return 0;
    }
    ti.cleanQueue();
    ti.runQueue();
    sleep(10);
    boost::mutex::scoped_lock lock(this->countMutex);
    this->useCount++;
  }
}


