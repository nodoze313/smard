/***************************************************************************
*   Copyright (C) 2008 by BluegrassNet                                    *
*   Written by James MacLachlan (james@bluegrass.net)                     *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/
#ifndef AUTORESPONDER_WORKER_H
#define AUTORESPONDER_WORKER_H


#include <stdio.h>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include "timing.h"
#include "mailinterface.h"
#include "userinterface.h"
#include "log.h"

#ifndef THREAD_LIFE
#define THREAD_LIFE 864
#endif


class worker
{
  const int* runtimeLogLevel;
  void* workerLoop();

public:
  worker(const int* runtimeLogLevel,const char* config);
  ~worker();

  int getUseCount();
  void setUseCount(int useCount);
  int initWorker();

  boost::thread* workerThread;
  const char* config;
  int useCount;
  boost::mutex countMutex;
};



#endif

