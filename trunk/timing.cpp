/***************************************************************************
 *   Copyright (C) 2008 by BluegrassNet                                    *
 *   Written by James MacLachlan (james@bluegrass.net)                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "timing.h"


timing::timing(mailInterface* mi, userInterface* ui,const char* config)
{
  openlog("autoresponder",LOG_CONS|LOG_PID,LOG_MAIL);
  MI = mi;
  UI = ui;
  //force update of vector
  lastQueueUpdate = time(0) - (QUEUETIMING + 10);
  lastQueueClean = time(0) - (QUEUETIMING + 10);

  //load config
  Config cfg;
  //open the config file
  cfg.readFile(config);
  //set maildir root
  checkFrequency = (int)cfg.lookup("check_frequency");
  runtimeLogLevel = (int)cfg.lookup("log_level");
  DBGLOGMSG("Starting timing");
}
timing::~timing()
{
  closelog();
}

int timing::runQueue()
{
  DBGLOGMSG("Running queue");
  time_t now = time(0);
  for(VACATIONINFO::iterator vacationIterator = UI->mVacationInfo.begin();vacationIterator != UI->mVacationInfo.end();vacationIterator++)
  {
    vacationInfo tvi = *vacationIterator;
    if(tvi.lastCheck + checkFrequency < now && tvi.start < now && tvi.stop > now)
    {
      MI->parseNewMessages(tvi.email,tvi.body,tvi.subject);
      tvi.lastCheck = time(0);
    }
  }
  return 0;
}

int timing::updateQueue()
{
  DBGLOGMSG("Updating queue");
  if(lastQueueUpdate + QUEUETIMING < time(0))
  {
    if(UI->checkJobList())
    {
      //the code should exit here
      LOGWOCODE(this->runtimeLogLevel,LOG_ERR,"Failed to check joblist");
      return 1;
    }
    lastQueueUpdate = time(0);
  }
  return 0;
}

int timing::cleanQueue()
{
  DBGLOGMSG("Cleaning queue");
  if(lastQueueClean + QUEUETIMING < time(0))
  {
    UI->clearDeadJobs();
    lastQueueClean = time(0);
  }
  return 0;
}



