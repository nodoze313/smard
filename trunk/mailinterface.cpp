/***************************************************************************
 *   Copyright (C) 2008 by BluegrassNet                                    *
 *   Written by James MacLachlan (james@bluegrass.net)                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "mailinterface.h"

mailInterface::mailInterface(const char* config)
{
  Config cfg;
  //open the config file
  cfg.readFile(config);
  //set maildir root
  maildirRoot = (const char*)cfg.lookup("maildir_location");
  //set maildir directory format
  maildirFormat = (const char*)cfg.lookup("maildir_format");
  //set DB location
  dbLocation = (const char*)cfg.lookup("db_location");
  //set max replies
  maxReplies = (int)cfg.lookup("max_replies");
  //SMTP server
  SMTPServer = (const char*)cfg.lookup("smtp_server");
  //SMTP port
  SMTPPort = (int)cfg.lookup("smtp_port");
  //log level
  runtimeLogLevel = (int)cfg.lookup("log_level");
  DBGLOGMSG("Started mailInterface");
};

mailInterface::~mailInterface()
{
  closelog();
}

int mailInterface::parseNewMessages(std::string userAddress,std::string replyMessage,std::string replySubject)
{
  int res;
  sqlite3* store;
  bool setupTables = false;
  char* errorMessage;
  //get a list of files in the directory we use the directory contents as the queue
  //this is probably not the best way to approach this, should use a real queue
  DIR *dp;
  struct dirent* ep;
  std::string newDir = maildirRoot + userAddress.substr(0,1) + "/" + userAddress + "/new";
  dp = opendir(newDir.c_str());
  if(dp)
  {
    //for each file in the new directory see if it has been read
    dirent *fp;
    int repliesSent = 0;
    while((fp = readdir(dp)) && repliesSent <= maxReplies)
    {
      //get the file name
      std::string fn = fp->d_name;
      if(fn.length() > 2)
      {
        std::string userDB = dbLocation + userAddress + ".db";
        //see if the users db exists
        struct stat s;
        if(stat(userDB.c_str(),&s) == -1)
          setupTables = true;
        //if not set tables to be generated
        //open the database for the user
        if(res = (sqlite3_open(userDB.c_str(), &store)) != SQLITE_OK)
        {
          LOGWCODE(runtimeLogLevel,LOG_ERR,"Failed to open DB, error: %s",sqlite3_errmsg(store));
          sqlite3_close(store);
          return res;
        }
        //if tables are not setup install them
        if(setupTables)
        {
          res = sqlite3_exec(store,GENERATE_TABLE_EMAIL,0,0,&errorMessage);
          if(res != SQLITE_OK) {
            LOGWCODE(runtimeLogLevel,LOG_ERR,"Failed to execute query, error: %s",errorMessage);
            sqlite3_free(errorMessage);
          }
          res = sqlite3_exec(store,GENERATE_TABLE_SENDER,0,0,&errorMessage);
          if(res != SQLITE_OK) {
            LOGWCODE(runtimeLogLevel,LOG_ERR,"Failed to execute query, error: %s",errorMessage);
            sqlite3_free(errorMessage);
          }
        }
        callbackInfo* ci = new callbackInfo();
        ci->countResult = -2;
        std::string findEmail = FIND_EMAIL + fn + "'";
        res = sqlite3_exec(store,findEmail.c_str(),lookupEmail,(void*)ci,&errorMessage);
        if(res != SQLITE_OK)
        {
          LOGWCODE(runtimeLogLevel,LOG_ERR,"Failed to execute query, error: %s",errorMessage);
          sqlite3_free(errorMessage);
        }
        if(ci->countResult == 0)
        {
          DBGLOGCDE("File not found in database, opening: %s",fn.c_str());
          std::string senderAddress = readMessage(fn,userAddress);
          if(senderAddress.length() >= 4)
          {
            std::string sSenderAddress = senderAddress;
            replace_all(sSenderAddress,"@","\\@");
            //add the email to the database
            std::string saveEmail = SAVE_EMAIL + fn + "','" + sSenderAddress + "')";
            DBGLOGCDE("Save Email Query: %s",saveEmail.c_str());
            res = sqlite3_exec(store,saveEmail.c_str(),0,0,&errorMessage);
            if(res != SQLITE_OK)
              LOGWCODE(runtimeLogLevel,LOG_ERR,"Failed to execute query, error: Message: %s",errorMessage);

            //check to see if the sender has been replied to
            std::string findSender = FIND_SENDER + sSenderAddress + "'";
            DBGLOGCDE("Find Sender Query: %s",findSender.c_str());
            ci->countResult = -2;
            res = sqlite3_exec(store,findSender.c_str(),lookupSender,(void*)ci,&errorMessage);
            if(res != SQLITE_OK)
              LOGWCODE(runtimeLogLevel,LOG_ERR,"Failed to execute query, error: Message: %s",errorMessage);
            if((ci->countResult == 0) && (sSenderAddress.compare("list") != 0))
            {
              repliesSent++;
              //send out the email
              sendMessage(senderAddress,userAddress,replyMessage,replySubject);
              //add the senderAddress to the db
              std::string saveSender = SAVE_SENDER + sSenderAddress + "')";
              DBGLOGCDE("Save Sender Query: %s",saveSender.c_str());
              res = sqlite3_exec(store,saveSender.c_str(),0,0,&errorMessage);
              if(res != SQLITE_OK)
                LOGWCODE(runtimeLogLevel,LOG_ERR,"Failed to execute query, error: Message: %s",errorMessage);
            }
          }
        }
        sqlite3_close(store);
        delete ci;
      }
    }
    closedir(dp);
  }
}

std::string mailInterface::readMessage(std::string message,std::string userAddress)
{
  std::string tmp, address;
  //open the file
  std::string newDir = maildirRoot + userAddress.substr(0,1) + "/" + userAddress + "/new/";
  message = newDir + message;
  FILE *messageHandle = fopen(message.c_str(),"r");
  if(messageHandle)
  {
    //find the first instance of "From"
    char* buffer = new char[1000];
    while(!feof(messageHandle))
    {
      fgets(buffer,1000,messageHandle);
      tmp = buffer;
      to_lower(tmp);
      //check to see if it is a list post, then don't reply
      if(tmp.find("list-") != tmp.npos)
      {
        DBGLOGCDE("This is a list: %s",tmp.c_str());
        address = "list";
        break;
      }
      //find from address
      int fromAddressPosition = tmp.find("from:");
      if(fromAddressPosition >= 0)
      {
        int loc = 0;
        if((loc = tmp.find(":")+1)>0)
          tmp.erase(0,loc);
        if((loc = tmp.find("<")+1)>0)
          tmp.erase(0,loc);
        if((loc = tmp.find(">"))>0)
          tmp.erase(loc);
        trim(tmp);
        address = tmp;
      }
      if(tmp.find("\r") == 0 || tmp.find("\n") == 0)
      {
        DBGLOGMSG("Found end of header");
        break;
      }
      memset(buffer,0,1000);
    }
    fclose(messageHandle);
  }
  else
  {
    LOGWCODE(runtimeLogLevel,LOG_ERR,"Failed to open message: %s. Check permissions and existence of file.",message.c_str());
    return "";
  }
  DBGLOGCDE("From Address:'%s'",address.c_str());
  return address;
}

int mailInterface::sendMessage(std::string toAddress,std::string fromAddress,std::string replyMessage,std::string replySubject)
{
  DBGLOGMSG("About to send message");
  /* New version with poco for SMTP traffic*/
  SMTPClientSession *SMTPConnection = new SMTPClientSession(SMTPServer.c_str(),SMTPPort);
  try
  {
    SMTPConnection->login();
  }
  catch(SMTPException e)
  {
    LOGWCODE(runtimeLogLevel,LOG_ERR,"Exception thrown: ",e.message().c_str());
  }
  catch(NetException e)
  {
    LOGWCODE(runtimeLogLevel,LOG_ERR,"Exception thrown: ",e.message().c_str());
  }
  MailMessage *message = new MailMessage();
  message->setSender(fromAddress);
  MailRecipient recipientAddress(MailRecipient::PRIMARY_RECIPIENT,toAddress);
  message->addRecipient(recipientAddress);
  message->setSubject(replySubject);
  //PartSource *messageBody = new PartSource(&replyMessage);
  //message->addPart(messageBody);
  message->setContent(replyMessage);
  try
  {
    SMTPConnection->sendMessage(*message);
  }
  catch(SMTPException e)
  {
    LOGWCODE(runtimeLogLevel,LOG_ERR,"Exception thrown: ",e.message().c_str());
  }
  catch(NetException e)
  {
    LOGWCODE(runtimeLogLevel,LOG_ERR,"Exception thrown: ",e.message().c_str());
  }
  SMTPConnection->close();
  DBGLOGMSG("Sent message");
  delete message;
  delete SMTPConnection;
  return 0;
}

int lookupEmail(void* arg, int argc, char** argv, char** colName)
{
  callbackInfo* ci = (callbackInfo*)arg;
  ci->countResult = atoi(argv[0]);
  return 0;
}

int lookupSender(void* arg, int argc, char** argv, char** colName)
{
  callbackInfo* ci = (callbackInfo*)arg;
  ci->countResult = atoi(argv[0]);
  return 0;
}
