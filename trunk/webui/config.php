<?php
// Language config
// Language files are located in './languages', change as required..
$CONF['default_language'] = 'en';

// Database Config
// mysql = MySQL 3.23 and 4.0
// mysqli = MySQL 4.1
// pgsql = PostgreSQL
$CONF['database_type'] = 'mysql';
$CONF['database_host'] = 'localhost';
$CONF['database_user'] = 'postfix';
$CONF['database_password'] = 'password';
$CONF['database_name'] = 'postfix';
$CONF['database_prefix'] = '';

// Here, if you need, you can customize table names.
$CONF['database_prefix'] = '';
$CONF['database_tables'] = array (
  'admin' => 'admin',
  'alias' => 'alias',
  'config' => 'config',
  'domain' => 'domain',
  'domain_admins' => 'domain_admins',
  'log' => 'log',
  'mailbox' => 'mailbox',
  'vacation' => 'vacation',
  'vacation_notification' => 'vacation_notification',
);

// Site Admin
// Define the Site Admins email address below.
// This will be used to send emails from to create mailboxes.
$CONF['admin_email'] = 'postmaster@bluegrass.net';

// Mail Server
// Hostname (FQDN) of your mail server.
// This is used to send email to Postfix in order to create mailboxes.
$CONF['smtp_server'] = 'maildata';
$CONF['smtp_port'] = '20025';

// Encrypt
// In what way do you want the passwords to be crypted?
// md5crypt = internal postfix admin md5
// md5 = md5 sum of the password
// system = whatever you have set as your PHP system default
// cleartext = clear text passwords (ouch!)
$CONF['encrypt'] = 'md5crypt';


//Or use a Posfixadmin config
//require_once('/path/to/postfixadmin/config.inc.php');

?>