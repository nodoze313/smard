<?php
require_once('config.php');
require_once('DB.php');

function pacrypt($username,$pw,$utable,&$db)
{
  //check to thier password
  $q = "SELECT password FROM ".$utable." WHERE username = '".$username."'";
  $login = $db->getOne($q);
  if($login)
  {
    $split_salt = preg_split('/\$/', $login);
    if (isset($split_salt[2]))
    {
        $salt = $split_salt[2];
    }
    $password = md5crypt($pw, $salt);
    $q = "SELECT count(*) FROM ".$utable." WHERE username = '".$username."' AND password = '".$password."'";
    $goodauth = $db->getOne($q);
    return $goodauth;
  }
  return 0;
}

function md5crypt ($pw, $salt="", $magic="")
{
   $MAGIC = "$1$";

   if ($magic == "") $magic = $MAGIC;
   if ($salt == "") $salt = create_salt ();
   $slist = explode ("$", $salt);
   if ($slist[0] == "1") $salt = $slist[1];

   $salt = substr ($salt, 0, 8);
   $ctx = $pw . $magic . $salt;
   $final = hex2bin (md5 ($pw . $salt . $pw));

   for ($i=strlen ($pw); $i>0; $i-=16)
   {
      if ($i > 16)
      {
         $ctx .= substr ($final,0,16);
      }
      else
      {
         $ctx .= substr ($final,0,$i);
      }
   }
   $i = strlen ($pw);
   while ($i > 0)
   {
      if ($i & 1) $ctx .= chr (0);
      else $ctx .= $pw[0];
      $i = $i >> 1;
   }
   $final = hex2bin (md5 ($ctx));

   for ($i=0;$i<1000;$i++)
   {
      $ctx1 = "";
      if ($i & 1)
      {
         $ctx1 .= $pw;
      }
      else
      {
         $ctx1 .= substr ($final,0,16);
      }
      if ($i % 3) $ctx1 .= $salt;
      if ($i % 7) $ctx1 .= $pw;
      if ($i & 1)
      {
         $ctx1 .= substr ($final,0,16);
      }
      else
      {
         $ctx1 .= $pw;
      }
      $final = hex2bin (md5 ($ctx1));
   }
   $passwd = "";
   $passwd .= to64 (((ord ($final[0]) << 16) | (ord ($final[6]) << 8) | (ord ($final[12]))), 4);
   $passwd .= to64 (((ord ($final[1]) << 16) | (ord ($final[7]) << 8) | (ord ($final[13]))), 4);
   $passwd .= to64 (((ord ($final[2]) << 16) | (ord ($final[8]) << 8) | (ord ($final[14]))), 4);
   $passwd .= to64 (((ord ($final[3]) << 16) | (ord ($final[9]) << 8) | (ord ($final[15]))), 4);
   $passwd .= to64 (((ord ($final[4]) << 16) | (ord ($final[10]) << 8) | (ord ($final[5]))), 4);
   $passwd .= to64 (ord ($final[11]), 2);
   return "$magic$salt\$$passwd";
}
function create_salt ()
{
   srand ((double) microtime ()*1000000);
   $salt = substr (md5 (rand (0,9999999)), 0, 8);
   return $salt;
}

function hex2bin ($str)
{
   $len = strlen ($str);
   $nstr = "";
   for ($i=0;$i<$len;$i+=2)
   {
      $num = sscanf (substr ($str,$i,2), "%x");
      $nstr.=chr ($num[0]);
   }
   return $nstr;
}
function to64 ($v, $n)
{
   $ITOA64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
   $ret = "";
   while (($n - 1) >= 0)
   {
      $n--;
      $ret .= $ITOA64[$v & 0x3f];
      $v = $v >> 6;
   }
   return $ret;
}


$dsn = $CONF['database_type']."://".$CONF['database_user'].":".$CONF['database_password']."@".$CONF['database_host']."/".$CONF['database_name'];
$vtable = $CONF['database_tables']['vacation'];
$utable = $CONF['database_tables']['mailbox'];
$username = $_POST['username'];
$fSubject = $_POST['fSubject'];
$fBody = $_POST['fBody'];
$switch = isset($_POST['enabled'])?1:0;
$pw = $_POST['password'];

$db = DB::CONNECT($dsn);
if(DB::isError($db))
{
  echo $db->message;
}
else
if($_POST['action'] == 'update')
{
    if(pacrypt($username,$pw,$utable,$db))
    {
      //update the database
      //check for an existing entry
      $q = "SELECT COUNT(*) FROM ".$vtable." WHERE email = '".$username."'";
      $entry = $db->getOne($q);
      if(DB::isError($entry))
        echo "Database error: ".$entry->message;
      else
      if($entry)
      {
        $q = "UPDATE ".$vtable." SET active = '".$switch."',subject = '".$fSubject."',body = '".$fBody."' WHERE email = '".$username."'";
        $db->query($q);
      }
      else
      {
        $q = "INSERT INTO ".$vtable." (email,subject,body,active) VALUES ('".$username."','".$fSubject."','".$fBody."','".$switch."')";
        $db->query($q);
      }
      echo "Applied successfully on: ".date('r',time());
    }
    else
      echo "Bad username or password";
}

if($_POST['action'] == 'load' || $_POST['action'] == 'update')
{
  if(pacrypt($username,$pw,$utable,$db))
  {
    $q = "SELECT * FROM ".$vtable." WHERE email = '".$username."'";
    $db->setFetchMode(DB_FETCHMODE_ASSOC);
    $result = $db->getRow($q);
    if(DB::isError($result))
      echo "Database error: ".$result->message."<br><br>Query: ".$q;
    else
    if($result)
    {
      echo "<form id='info_form' action='action.php' method='post' onsubmit='saveSettings();return false;'><input type='hidden' name='action' value='update'>Enabled: <input type='checkbox' name='enabled' ";
      echo $result['active']?"checked":"";
      echo " ><br><br>Subject:<br><textarea class=\"flat\" cols=\"60\" name=\"fSubject\">";
      echo $result['subject'];
      echo "</textarea><br><br>Body:<br><textarea class=\"flat\" rows=\"10\" cols=\"60\" name=\"fBody\">";
      echo $result['body'];
      echo "</textarea><br><br><input type='submit' value='Update'><br></form>";
    }
    else
      echo "Database query problem";
  }
  else
    echo "Bad username or password";
}
$db->disconnect();

?>