/***************************************************************************
 *   Copyright (C) 2008 by BluegrassNet                                    *
 *   Written by James MacLachlan (james@bluegrass.net)                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef TIMING_H
#define TIMING_H

#include <time.h>

#include <vector>

#include <libconfig.h++>

#include "mailinterface.h"
#include "userinterface.h"
#include "log.h"

using namespace libconfig;

#define QUEUETIMING 10

class timing {
  int lastQueueClean;
  int lastQueueUpdate;
  int checkFrequency;
  int runtimeLogLevel;
  
  mailInterface* MI;
  userInterface* UI;

public:
  timing(mailInterface* mi, userInterface* ui,const char* config);
  ~timing();

  int runQueue();
  int updateQueue();
  int cleanQueue();
};


#endif

