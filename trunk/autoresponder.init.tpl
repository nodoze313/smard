#!/bin/bash
#
# autoresponder        This shell script takes care of starting and stopping
#               the autoresponder subsystem (mysqld).
#
# chkconfig: - 345 80 05
# description:  Autoresponder daemon.
# processname: autoresponder
# config: /etc/autoresponder.con
# pidfile: /var/run/autoresponder.pid
# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network
start(){
  /usr/sbin/autoresponder -d
  [ $? -eq 0 ] && touch /var/lock/subsys/autoresponder
  return $?
}
stop(){
  kill $(cat __STATE__)
  [ $? -eq 0 ] && rm /var/lock/subsys/autoresponder
  return $?
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  status)
    status autoresponder
    ;;
  restart)
    stop
    start
    ;;
  *)
    echo $"Usage: $0 {start|stop|status|restart}"
    exit 1
esac

exit $?

