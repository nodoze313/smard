/***************************************************************************
 *   Copyright (C) 2008 by BluegrassNet                                    *
 *   Written by James MacLachlan (james@bluegrass.net)                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef MAILINTERFACE_H
#define MAILINTERFACE_H

#include <sqlite3.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <dirent.h>
#include <sys/stat.h>
#include <syslog.h>

#include <boost/algorithm/string.hpp>

#include <libconfig.h++>

#include <Poco/Net/SMTPClientSession.h>
#include <Poco/Net/MailMessage.h>
#include <Poco/Net/MailRecipient.h>
#include <Poco/Net/PartSource.h>
#include <Poco/Net/NetException.h>

#include "log.h"

#define _LINE printf("Line: %i\n", __LINE__);

using namespace boost::algorithm;
using namespace libconfig;
using namespace Poco::Net;


//Queries for SQLite
#define GENERATE_TABLE_SENDER "CREATE TABLE sender (emailAddress VARCHAR(128) PRIMARY KEY,reply DATE);"
#define GENERATE_TABLE_EMAIL "CREATE TABLE email (emailGUID VARCHAR(128) PRIMARY KEY,senderAddress VARCHAR(128),read DATE);"
#define FIND_SENDER "SELECT COUNT(*) FROM sender WHERE emailAddress = '"
#define FIND_EMAIL "SELECT COUNT(*) FROM email WHERE emailGUID = '"
#define SAVE_SENDER "INSERT INTO sender (emailAddress) VALUES ('"
#define SAVE_EMAIL "INSERT INTO email(emailGUID,senderAddress) VALUES ('"

//SQLite callbacks
//lookup email
int lookupEmail(void* arg, int argc, char** argv, char** colName);
//lookup sender
int lookupSender(void* arg, int argc, char** argv, char** colName);

class callbackInfo {
public:
  int countResult;
};

class mailInterface {
  std::string maildirRoot;
  std::string maildirFormat;
  std::string dbLocation;
  std::string SMTPServer;
  int SMTPPort;
  int maxReplies;
  int runtimeLogLevel;

  //return the from address
  std::string readMessage(std::string message,std::string userAddress);
  //send the out of office email
  int sendMessage(std::string toAddress,std::string fromAddress,std::string replyMessage,std::string replySubject);

public:
  mailInterface(const char* config);
  ~mailInterface();

  int parseNewMessages(std::string address,std::string replyMessage,std::string replySubject);
};

#endif


