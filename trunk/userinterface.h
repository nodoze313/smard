/***************************************************************************
 *   Copyright (C) 2008 by BluegrassNet                                    *
 *   Written by James MacLachlan (james@bluegrass.net)                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <syslog.h>

#include <string>
#include <vector>

#include <libconfig.h++>

#define MYSQLPP_MYSQL_HEADERS_BURIED
#include <mysql++/mysql++.h>

#include "log.h"

using namespace std;
using namespace libconfig;

#define VACATIONINFO vector<vacationInfo>

#define _LINE printf("Line: %i\n", __LINE__);

struct vacationInfo {
  string email;
  string subject;
  string body;
  time_t start;
  time_t stop;
  time_t lastCheck;
};

class userInterface {
  //config variables
  string myHost;
  string myUser;
  string myPass;
  string viDatabase;
  string viQuery;
  string dbLocation;
  int runtimeLogLevel;
  
public:
  userInterface(const char* config);
  ~userInterface();

  //vacation info from MySQL
  VACATIONINFO mVacationInfo;
  int checkJobList();
  int clearDeadJobs();
};



#endif

