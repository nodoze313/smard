/***************************************************************************
 *   Copyright (C) 2008 by BluegrassNet                                    *
 *   Written by James MacLachlan (james@bluegrass.net)                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <string.h>
#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>

#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>

#ifndef CONFIG_LOCATION
#define CONFIG_LOCATION "/etc/autoresponder.conf"
#endif

#include <libconfig.h++>

#include "worker.h"
#include "log.h"

using namespace boost::algorithm;
using namespace libconfig;

int main(int argc, char** argv)
{
  int daemon = 0;
  int* runtimeLogLevel = new int(7);
  int error = 0;
  passwd* dUser = 0;
  bool configParsed = false;
  std::string* config = new std::string(CONFIG_LOCATION);
  /* Get CLI options */
  for(int argp = 1;argc != argp;argp++)
  {
    if(argv[argp][0] == '-')
    {
      for(int argvp = 1;argvp < strlen(argv[argp]);argvp++)
      {
        if(argv[argp][argvp] == 'd')
        {
          daemon = 1;
          argvp++;
        }
        if(argv[argp][argvp] == 'h')
        {
          printf("\nUsage:\t-d\t\t\tUse daemon mode\n\t-c /path/to/config\tConfig file\n\t-h\t\t\tShow this help\n\n");
          return 0;
        }
        if(argv[argp][argvp] == 'c')
        {
          char* confStr = &argv[argp][argvp];
          if(strlen(confStr) > 3)
          {
            config->assign(++confStr);
            Config cfg;
            cfg.readFile(config->c_str());
            configParsed = true;
            *runtimeLogLevel = (int)cfg.lookup("log_level");
            const char* dUsername = (const char*)cfg.lookup("daemon_user");
            dUser = getpwnam(dUsername);
          }
          else if(argv[argp+1])
          {
            confStr = &argv[++argp][0];
            config->assign(confStr);
            Config cfg;
            cfg.readFile(config->c_str());
            configParsed = true;
            *runtimeLogLevel = (int)cfg.lookup("log_level");
            const char* dUsername = (const char*)cfg.lookup("daemon_user");
            dUser = getpwnam(dUsername);
          }
          else
          {
            printf("\nPlease specify a config location when using -c.\n");
            LOGWOCODE(*runtimeLogLevel,LOG_ERR,"Please specify a config location when using -c.");
            return 0;
          }
        }
      }
    }
  }
  if(!configParsed)
  {
    Config cfg;
    cfg.readFile(config->c_str());
    configParsed = true;
    *runtimeLogLevel = (int)cfg.lookup("log_level");
    const char* dUsername = (const char*)cfg.lookup("daemon_user");
    dUser = getpwnam(dUsername);
  }
  DBGLOGCDE("Daemon: %d",daemon);
  DBGLOGCDE("Config: %s",config->c_str());
  DBGLOGCDE("Daemon User: %s",dUser->pw_name);
  DBGLOGCDE("Daemon uid: %i",dUser->pw_uid);
  DBGLOGCDE("Log Level: %i",*runtimeLogLevel);
  if(daemon)
  {
    if(fork()) return 0;
    if(setuid(dUser->pw_uid))
    {
      printf("Failed to run as user %s - %i, please check that the user exists\n",dUser->pw_name,dUser->pw_uid);
      LOGWCODE(*runtimeLogLevel,LOG_ERR,"Failed to run as user %s, please check that the user exists\n",dUser->pw_name);
      exit(1);
    }
    chdir(getenv("HOME"));
    setsid();
    umask(0);
    int pid = 0;
    pid = fork();
    if(pid)
    {
      LOGWCODE(*runtimeLogLevel,LOG_NOTICE,"Running as daemon, PID: %i",pid);
      FILE* pidFile = fopen(STATE,"w+");
      if(pidFile)
      {
        char* pidBuf = new char(32);
        if(sprintf(pidBuf,"%i",pid))
        {
          fputs(pidBuf,pidFile);
        }
        fclose(pidFile);
      }
      else
      {
        LOGWCODE(*runtimeLogLevel,LOG_WARNING,"Failed to open for writing pid file: %i.  Please check permissions.",pid);
        return 1;
      }
      return 0;
    }
  }
  //make sure paths are still valid after changing chdir
  struct stat configStatus;
  if(stat(config->c_str(),&configStatus) == -1)
  {
    printf("\nThe path specified for the config file is invalid.  Path:\n%s\n",config->c_str());
    LOGWCODE(*runtimeLogLevel,LOG_ERR,"The path specified for the config file is invalid.  Path: %s ",config->c_str());
    return 1;
  }
  //create worker thread
  worker* iDoWork = new worker((const int*)runtimeLogLevel,config->c_str());
  //iDoWork->setUseCount(THREAD_LIFE);
  if(iDoWork->initWorker())
    return 1;
  while(1)
  {
    if(iDoWork->getUseCount() >= THREAD_LIFE)
    {
      iDoWork->setUseCount(0);
      if(iDoWork->workerThread)
        iDoWork->workerThread->join();
      if(iDoWork->initWorker())
        return 1;
    }
    else if(iDoWork->getUseCount() == -1)
      return 1;
    sleep(10);
  }
  
  return 0;
}



