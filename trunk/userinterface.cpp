/***************************************************************************
 *   Copyright (C) 2008 by BluegrassNet                                    *
 *   Written by James MacLachlan (james@bluegrass.net)                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "userinterface.h"

userInterface::userInterface(const char* config)
{
  openlog("autoresponder",LOG_CONS|LOG_PID,LOG_MAIL);
  Config cfg;
  //open the config file
  cfg.readFile(config);
  //set maildir root
  myHost = (const char*)cfg.lookup("mysql_host");
  myUser = (const char*)cfg.lookup("mysql_user");
  myPass = (const char*)cfg.lookup("mysql_pass");
  viDatabase = (const char*)cfg.lookup("mysql_database");
  viQuery = (const char*)cfg.lookup("mysql_vacation_query");
  dbLocation = (const char*)cfg.lookup("db_location");
  this->runtimeLogLevel = (int)cfg.lookup("log_level");
  DBGLOGMSG("Started userInterface");
}
userInterface::~userInterface()
{
  closelog();
}

int userInterface::checkJobList()
{
  mVacationInfo.clear();
  mysqlpp::Connection *con = new mysqlpp::Connection();
  try
  {
    con->connect(viDatabase.c_str(),myHost.c_str(),myUser.c_str(),myPass.c_str());
  }
  catch(int e)
  {
    //The error is here, but nothing is written
    LOGWCODE(this->runtimeLogLevel,LOG_ERR,"Failed to connect to database, exception caught: %i",e);
    return 1;
  }
  if(con->connected())
  {
    vacationInfo vi;
    mysqlpp::Query query = con->query();
    query << viQuery.c_str();
    mysqlpp::StoreQueryResult res = query.store();
    con->disconnect();
    int resPos;
    for(resPos = 0;res.num_rows() > resPos;resPos++)
    {
      vi.email = (string)res[resPos][0];
      vi.subject = (string)res[resPos][1];
      vi.body = (string)res[resPos][2];
      if(res[resPos][3])
        vi.start = (time_t)res[resPos][3];
      else
        vi.start = time(0);
      if(res[resPos][4])
        vi.stop = (time_t)res[resPos][4];
      else
        vi.stop = vi.start + 86400 * 30;
      vi.lastCheck = time(0);
      mVacationInfo.push_back(vi);
    }
  }
  else
  {
    LOGWCODE(runtimeLogLevel,LOG_ERR,"MySQL Error: %s",con->error());
    return 1;
  }
  delete con;
  return 0;
}

int userInterface::clearDeadJobs()
{
  //get a list of the existing dbs
  DIR *dp;
  struct dirent* ep;
  string userDB = dbLocation;
  dp = opendir(dbLocation.c_str());
  if(dp)
  {
    //for each file in the new directory see if it has been read
    dirent *fp;
    while(fp = readdir(dp))
    {
      //get the file name
      string fn = fp->d_name;
      if(fn.length() > 2)
      {
        int found = 0;
        for(VACATIONINFO::iterator vacationIterator = mVacationInfo.begin();vacationIterator != mVacationInfo.end();vacationIterator++)
        {
          vacationInfo tvi = *vacationIterator;
          if(tvi.email+".db" == fn)
          {
            found = 1;
            break;
          }
        }
        if(!found)
        {
          //delete the file
          string fullPath = dbLocation + fn;
          LOGWOCODE(runtimeLogLevel,LOG_ERR,"Failed to delete db file");
          remove(fullPath.c_str());
        }
      }
    }
    closedir(dp);
  }
}


